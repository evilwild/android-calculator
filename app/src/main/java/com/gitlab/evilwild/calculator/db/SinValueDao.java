package com.gitlab.evilwild.calculator.db;

import com.gitlab.evilwild.calculator.dto.SinValue;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SinValueDao {
    @Query("SELECT * FROM SinValue")
    List<SinValue> getAll();

    @Insert
    void insertAll(SinValue... sinValues);

    @Delete
    void delete(SinValue sinValue);

    @Query("DELETE FROM SinValue")
    void deleteAll();

    @Query("SELECT * FROM SinValue WHERE y = (SELECT MAX(Y) FROM SinValue)")
    List<SinValue> getAllMax();

    @Query("SELECT * FROM SinValue WHERE y = (SELECT MIN(Y) FROM SinValue)")
    List<SinValue> getAllMin();
}
