package com.gitlab.evilwild.calculator.math;

public interface ICalculator {
    // A + B
    double add(double a, double b);
    // A - B
    double sub(double a, double b);
    // A * B
    double mul(double a, double b);
    // A ^ B
    double pow(double a, double b);
    // A / B
    double div(double a, double b);
    // A % B
    double percent(double a, double b);
}
