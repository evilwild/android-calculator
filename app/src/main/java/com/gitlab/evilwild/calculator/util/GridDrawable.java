package com.gitlab.evilwild.calculator.util;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gitlab.evilwild.calculator.db.SinValueDao;
import com.gitlab.evilwild.calculator.db.SinValueDatabase;
import com.gitlab.evilwild.calculator.dto.SinValue;
import com.gitlab.evilwild.calculator.dto.SinValuesOptions;
import com.gitlab.evilwild.calculator.math.Functions;

import java.util.List;

public class GridDrawable extends Drawable {

    private Paint pointPaint;
    private Paint axisPaint;
    private Paint gridPaint;
    private Paint extremumPaint;
    private final static int GRID_STEP = 30;
    private final SinValueDatabase db;

    public GridDrawable(SinValueDatabase db) {
        this.db = db;
        initPaints();
    }

    private void initPaints() {
        this.pointPaint = new Paint();
        this.pointPaint.setStrokeWidth(5f);
        this.pointPaint.setStyle(Paint.Style.STROKE);
        this.pointPaint.setARGB(255, 206, 35, 183);

        this.axisPaint = new Paint();
        this.axisPaint.setARGB(255, 120, 120, 120);
        this.axisPaint.setStrokeWidth(5f);

        this.gridPaint = new Paint();
        this.gridPaint.setStrokeWidth(3f);
        this.gridPaint.setARGB(255, 200, 200, 200);

        this.extremumPaint = new Paint();
        this.extremumPaint.setStrokeWidth(8f);
        this.extremumPaint.setARGB(255, 15, 193, 27);
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        drawAxisAndGrid(canvas);

        List<SinValue> sinValueList = Functions.calculateSinValues(
                SinValuesOptions.builder()
                        .centerX(this.getCenterX())
                        .centerY(this.getCenterY())
                        .stepX(0.1f)
                        .build()
        );


        SinValueDao sinValueDao = this.db.sinValueDao();
        sinValueDao.deleteAll();
        sinValueDao.insertAll(sinValueList.toArray(new SinValue[sinValueList.size()]));

        Path sinPath = generateSinPath(sinValueList);
        canvas.drawPath(sinPath, this.pointPaint);

        drawExtremumPoints(canvas);
    }

    private Path generateSinPath(List<SinValue> sinValues) {
        Path sinPath = new Path();

        boolean firstMove = true;

        for (SinValue sinValue : sinValues) {
            if (!firstMove) {
                sinPath.lineTo(sinValue.getX(), sinValue.getY());
            } else {
                sinPath.moveTo(sinValue.getX(), sinValue.getY());
                firstMove = false;
            }
        }

        return sinPath;
    }

    private void drawAxisAndGrid(@NonNull Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        int centerX = getCenterX();
        int centerY = getCenterY();
        // draw x grid below x axis
        for (int y = centerY + GRID_STEP; y < height; y += GRID_STEP) {
            canvas.drawLine(0f, y, width, y, this.gridPaint);
        }
        // draw x grid above x axis
        for (int y = centerY - GRID_STEP; y > 0; y -= GRID_STEP) {
            canvas.drawLine(0f, y, width, y, this.gridPaint);
        }
        // draw y grid after y axis
        for (int x = centerX + GRID_STEP; x < width; x += GRID_STEP) {
            canvas.drawLine(x, 0f, x, height, this.gridPaint);
        }
        // draw y grid before y axis
        for (int x = centerX - GRID_STEP; x > 0; x -= GRID_STEP) {
            canvas.drawLine(x, 0f, x, height, this.gridPaint);
        }
        // center x axis
        canvas.drawLine(0f, centerY, width, centerY, this.axisPaint);
        // center y axis
        canvas.drawLine(centerX, 0f, centerX, height, this.axisPaint);
    }

    private void drawExtremumPoints(Canvas canvas) {
        List<SinValue> maxList = this.db.sinValueDao().getAllMax();
        List<SinValue> minList = this.db.sinValueDao().getAllMin();

        for (SinValue value : maxList) {
            canvas.drawPoint(value.getX(), value.getY(), this.extremumPaint);
        }

        for (SinValue value : minList) {
            canvas.drawPoint(value.getX(), value.getY(), this.extremumPaint);
        }
    }

    private int getCenterX() {
        return getWidth() / 2;
    }

    private int getCenterY() {
        return getHeight() / 2;
    }

    private int getWidth() {
        return Math.abs(getBounds().width());
    }

    private int getHeight() {
        return Math.abs(getBounds().height());
    }

    @Override
    public void setAlpha(int alpha) {
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }
}
