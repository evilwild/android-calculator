package com.gitlab.evilwild.calculator;

import androidx.annotation.IdRes;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gitlab.evilwild.calculator.dto.CalculatorFields;
import com.gitlab.evilwild.calculator.math.CalculatorImpl;

import java.util.Locale;
import java.util.Optional;

public class CalculatorActivity extends AppCompatActivity {

    private static final String DEFAULT_INPUT_VALUE = "0.0";

    CalculatorImpl calculatorImpl;
    EditText inputA;
    EditText inputB;
    TextView resultLabel;
    AlertDialog numberAlert;
    AlertDialog nullDivisionAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        this.inputA = this.findViewById(R.id.inputA);
        this.inputA.setText(DEFAULT_INPUT_VALUE);
        this.inputB = this.findViewById(R.id.inputB);
        this.inputB.setText(DEFAULT_INPUT_VALUE);
        this.resultLabel = this.findViewById(R.id.resultTextView);
        this.resultLabel.setText(DEFAULT_INPUT_VALUE);

        this.numberAlert = getAlertExceptionDialog(R.string.exception_numeric_format);
        this.nullDivisionAlert = getAlertExceptionDialog(R.string.exception_null_division);

        this.calculatorImpl = new CalculatorImpl();
    }

    public void resetUserInputs() {
        this.inputA.setText(DEFAULT_INPUT_VALUE);
        this.inputB.setText(DEFAULT_INPUT_VALUE);
        this.resultLabel.setText(DEFAULT_INPUT_VALUE);
    }

    public Optional<CalculatorFields> getUserInputs() {
        try {
            return Optional.of(new CalculatorFields(
                    getDecimalNumberInput(this.inputA),
                    getDecimalNumberInput(this.inputB)));
        }  catch (NumberFormatException ex) {
            this.numberAlert.show();
            return Optional.empty();
        }
    }

    public void onCalculatorButtonClick(View calculatorButton) {
        Button button = (Button) calculatorButton;
        @IdRes int buttonId = button.getId();

        Optional<CalculatorFields> calculatorFieldsOpt = getUserInputs();

        if (!calculatorFieldsOpt.isPresent()) {
            return;
        }

        CalculatorFields calculatorFields = calculatorFieldsOpt.get();

        if (buttonId == R.id.sumButton) {
            setResultLabel(this.calculatorImpl.add(calculatorFields.a(), calculatorFields.b()));
        } else if (buttonId == R.id.divButton) {
            try {
                setResultLabel(this.calculatorImpl.div(calculatorFields.a(), calculatorFields.b()));
            } catch (IllegalArgumentException ex) {
                this.nullDivisionAlert.show();
            }
        } else if (buttonId == R.id.subButton) {
            setResultLabel(this.calculatorImpl.sub(calculatorFields.a(), calculatorFields.b()));
        } else if (buttonId == R.id.mulButton) {
            setResultLabel(this.calculatorImpl.mul(calculatorFields.a(), calculatorFields.b()));
        } else if (buttonId == R.id.percentButton) {
            setResultLabel(this.calculatorImpl.percent(calculatorFields.a(), calculatorFields.b()));
        } else if (buttonId == R.id.powButton) {
            setResultLabel(this.calculatorImpl.pow(calculatorFields.a(), calculatorFields.b()));
        } else if (buttonId == R.id.clearButton) {
            resetUserInputs();
        }
    }

    public void setResultLabel(double resultValue) {
        this.resultLabel.setText(String.format(Locale.ENGLISH, "%.4f", resultValue));
    }

    public double getDecimalNumberInput(EditText numberInput) {
        int givenType = numberInput.getInputType();
        int validType = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL;
        if ((givenType & validType) != validType) {
            throw new UnsupportedOperationException("Method is supported only for decimal number inputs");
        }

        String numberReprStr = numberInput.getText().toString();
        return Double.parseDouble(numberReprStr);
    }

    private AlertDialog getAlertExceptionDialog(@StringRes int msgRef) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CalculatorActivity.this);
        return builder.setTitle(R.string.exception_dialog)
                .setMessage(msgRef)
                .setCancelable(true)
                .setPositiveButton(R.string.exception_positive_button, (dialog, which) -> dialog.dismiss())
                .create();
    }
}