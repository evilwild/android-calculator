package com.gitlab.evilwild.calculator.math;

import com.gitlab.evilwild.calculator.dto.SinValue;
import com.gitlab.evilwild.calculator.dto.SinValuesOptions;

import java.util.ArrayList;
import java.util.List;

public class Functions {
    public static List<SinValue> calculateSinValues(SinValuesOptions options) {
        List<SinValue> list = new ArrayList<>();

        final int amplitude = 60;

        for (float x = -options.centerX(); x <= options.centerX(); x += options.stepX()) {
            float y = amplitude * (float) Math.sin(x * (Math.PI/180));
            list.add(SinValue.builder().x(x + options.centerX()).y(options.centerY() - y).build());
        }
        return list;
    }
}
