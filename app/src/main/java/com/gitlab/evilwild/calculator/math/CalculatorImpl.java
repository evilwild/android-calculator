package com.gitlab.evilwild.calculator.math;

public class CalculatorImpl implements ICalculator {
    @Override
    public double add(double a, double b) {
        return a + b;
    }

    @Override
    public double sub(double a, double b) {
        return a - b;
    }

    @Override
    public double mul(double a, double b) {
        return a * b;
    }

    @Override
    public double pow(double a, double b) {
        return Math.pow(a, b);
    }

    @Override
    public double div(double a, double b) {
        if (b == 0) {
            throw new IllegalArgumentException("Division on null");
        }
        return a / b;
    }

    @Override
    public double percent(double a, double b) {
        return Math.abs(this.mul(this.div(a, b),100));
    }
}