package com.gitlab.evilwild.calculator.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.gitlab.evilwild.calculator.dto.SinValue;

@Database(entities = {SinValue.class}, version = 1)
public abstract class SinValueDatabase extends RoomDatabase {
    private static final String DB_NAME = "sin_values";
    private static SinValueDatabase instance;

    public abstract SinValueDao sinValueDao();

    public static synchronized SinValueDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    SinValueDatabase.class,
                    DB_NAME)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
            .build();
        }
        return instance;
    }
}
